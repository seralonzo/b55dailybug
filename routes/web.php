<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dailybuglanding');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/allbugs', 'BugController@index');

Route::get('/indivbug/{id}', 'BugController@showIndivBug');

// User
//To show add bug Form
Route::get('/addbug', 'BugController@create');

//To save
Route::post('/addbug', 'BugController@store');

//To show user bugs
Route::get('/mybugs', 'BugController@indivBugs');

//To delete bug
Route::delete('/deletebug/{id}', 'BugController@destroy');

//To go to the edit form
Route::get('/editbug/{id}', 'BugController@edit');

//To save edited bug
Route::patch('/editbug/{id}', 'BugController@update');

//To save edited bug
Route::patch('/editbug/{id}', 'BugController@update');

//Admin Routes
//To go to solve form
Route::get('/solve/{id}', 'BugController@showSolve');

//To save
Route::post('/solve', 'BugController@saveSolution');

// To delete solution
Route::delete('/deletesolution/{id}', 'SolutionController@deleteSolution');

T